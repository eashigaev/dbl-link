const db = require('../data/db.json');

class Repository {

    fetchUserByName(name) {
        return db.find(el => el.name.toLowerCase() === name.toLowerCase());
    }

    fetchLinkByPath(name, path) {
        const user = this.fetchUserByName(name);
        const links = user.paths;
        const link = Object.keys(links).find(el => el.toLowerCase() === path.toLowerCase());
        return links[link];
    }

    fetchLinkByAlias(name, alias) {
        const user = this.fetchUserByName(name);
        const links = user.aliases;
        const link = Object.keys(links).find(el => el.toLowerCase() === alias.toLowerCase());
        return links[link];
    }

}

exports.repository = new Repository();

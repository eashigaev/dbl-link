class Utils {

    fetchUrlPath(url) {
        const matches = url.match(/https?:\/\/(www\.)?([^\?]+)/);
        return matches
            ? matches[2].replace(/\/$/g, '')
            : false;
    }

}

exports.utils = new Utils();

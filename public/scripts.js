const app = (() => {

    const elements = {
        toast: '.mdl-js-snackbar'
    };

    const utils = new class {

        showToast(message, selector = elements.toast) {
            var notification = document.querySelector(selector);
            notification.MaterialSnackbar.showSnackbar({ message });
        }

        redirect(url) {
            window.location.href = url;
            return false;
        }

    };

    return new class {

        followUrl(name, url) {
            utils.redirect(`/${name}/?url=${url}`);
            return false;
        }
    }

})();

$(() => {

    const elems = {
        'follow-form': '.follow-form',
        'follow-button': '.follow-button',
        'url-input': '#input',
        'name-input': '#name'
    }

    $(document)
        .on('submit', elems["follow-form"], () => {
            app.followUrl(
                $(elems["name-input"]).val(),
                $(elems["url-input"]).val()
            )
            return false;
        })
        .on('click', elems["follow-button"], () => $(elems["follow-form"]).submit())
        .on('focus', elems["url-input"], function() {
            $(this).val('');
        });
});
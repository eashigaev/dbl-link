const express = require('express')
const helmet = require('helmet')

const hbs  = require('hbs')

const {repository} = require('./app/persistence');
const {utils} = require('./app/infrastructure');

const app = express()

// add some security-related headers to the response
app.use(helmet())
// add static
app.use(express.static(__dirname + '/public'));

app.set('view engine', 'hbs');
hbs.registerPartials(__dirname + '/views');

app.get('/:name/:alias', (req, res) => {
    const {name, alias} = req.params;
    const link = repository.fetchLinkByAlias(
        name, alias
    );
    return link 
        ? res.redirect(link)
        : res.render('404');
});

app.get('/:name', (req, res) => {
    const {name} = req.params;
    const {url} = req.query;

    const handleUrl = (name, url) => {
        try {
            const path = utils.fetchUrlPath(url || '');
            const link = repository.fetchLinkByPath(
                name, path
            );
            if (!link) throw new Exception;
            return res.redirect(link);
        } catch (e) {
            return res.render('404');
        }
    };

    const handleProfile = (name) => {
        const user = repository.fetchUserByName(
            req.params.name
        );
        return user 
            ? res.render('user', {user})
            : res.render('404');
    };
    
    return url 
        ? handleUrl(name, url)
        : handleProfile(name);    
});

app.get('/', (req, res) => {
    return res.render('index');
});

app.listen(3000, function () {
    console.log('Example app listening on port 3000!');
});

module.exports = app
